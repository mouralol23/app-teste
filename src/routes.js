const express = require('express');
const routes = express.Router()
const request = require('request');
const v = require('./variaveis');
routes.get('/',async (req, res) => {
    res.status(200).send({Rotas:"/forward"})
})
routes.get("/forward", async (req, res) => {
    console.log('requisição recebida', new Date())
    if (v.path == "" || v.path == null) {
        res.status('500').send(`variável  path null`);
    } else if (v.word == "false" || v.word == "") {
        res.status('500').send(`variável  word False ou null`);
    } else {
        try {
            request((v.dst) == '' ? res.status(500).send('URL DE DST NULL') : v.dst, function (error, response, body) {
                try {
                    var cod = (response) == undefined ? "400" : response.statusCode
                    if (!error && cod == 200) {
                        res.send(`Aplicação Up ${cod}`);
                    } else {
                        res.status(cod).send({ status:"Aplicação Down, favor analisa"});
                    }
                } catch (err) {
                    res.status(cod).send({status:"Aplicação Down, favor analisa"})
                }
            });
        }
        catch (err) {
            console.log("erro", err);
        }
    }
})

module.exports = routes